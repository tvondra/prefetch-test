#!/bin/bash

DIR=$1
TIME=$2
DEVICE=$3

function run_test() {
	bin=$1
	fname=$2
	mode=$3
	workload=$4
	duration=$5
	queue_size=$6
	threads=$7
	sprob=$8
	cprob=$9
	size=${10}
	run=${11}

	sudo ./drop-caches.sh

	warmup_retval=""

	# in buffered mode we first run in warmup mode, until the results stop improving
	# we don't do this in DIRECT mode, because it'd be simply futile
	if [ "$mode" == "BUFFERED" ]; then

		iostat="iostat/$size/$bin-$mode-$workload-$duration-$queue_size-$threads-$sprob-$cprob-$run-warmup.log"
		result="results/$size/$bin-$mode-$workload-$duration-$queue_size-$threads-$sprob-$cprob-$run-warmup.log"

		iostat -x -k 1 > $iostat 2>&1 &
		iostat_pid=$!

		# will run until the results for a 5 second interval is not more than 5%
		# better than the preceding interval (so 10 seconds minimum)
		./$bin WARMUP $fname $mode $workload $duration $queue_size $threads $sprob $cprob > $result 2>&1

		# was the warmup successful?
		warmup_retval=$?

		# kill the iostat collection
		kill $iostat_pid > /dev/null 2>&1

	fi

	# now a run the actual test

	iostat="iostat/$size/$bin-$mode-$workload-$duration-$queue_size-$threads-$sprob-$cprob-$run-test.log"
	iostat_detail="iostat/$size/$bin-$mode-$workload-$duration-$queue_size-$threads-$sprob-$cprob-$run-test-details.log"
	result="results/$size/$bin-$mode-$workload-$duration-$queue_size-$threads-$sprob-$cprob-$run-test.log"

	cached_before=`grep '^Cached' /proc/meminfo | awk '{print $2}'`

	iostat -x -k $duration 2 > $iostat 2>&1 &
	iostat -x -k 1 > $iostat_detail 2>&1 &
	iostat_pid=$!

	start_time=`date +%s`

	./$bin TEST $fname $mode $workload $duration $queue_size $threads $sprob $cprob > $result 2>&1

	end_time=`date +%s`

	kill $iostat_pid

	# wait for the other iostat process to finish on its own
	wait

	cached_after=`grep '^Cached' /proc/meminfo | awk '{print $2}'`

	nrequests=`grep TOTAL $result | awk '{print $2}'`
	speed=`grep TOTAL $result | awk '{print $4}'`
	elapsed=`grep TOTAL $result | awk '{print $7}'`

	rps=`grep $DEVICE $iostat | tail -n1 | awk '{print $4}'`
	kbps=`grep $DEVICE $iostat | tail -n1 | awk '{print $6}'`
	avgrqsz=`grep $DEVICE $iostat | tail -n1 | awk '{print $8}'`
	avgqusz=`grep $DEVICE $iostat | tail -n1 | awk '{print $9}'`
	util=`grep $DEVICE $iostat | tail -n1 | awk '{print $14}'`

	cpu_user=`grep avg-cpu -A 1 $iostat | tail -n1 | awk '{print $1}'`
	cpu_nice=`grep avg-cpu -A 1 $iostat | tail -n1 | awk '{print $2}'`
	cpu_system=`grep avg-cpu -A 1 $iostat | tail -n1 | awk '{print $3}'`
	cpu_iowait=`grep avg-cpu -A 1 $iostat | tail -n1 | awk '{print $4}'`
	cpu_steal=`grep avg-cpu -A 1 $iostat | tail -n1 | awk '{print $5}'`
	cpu_idle=`grep avg-cpu -A 1 $iostat | tail -n1 | awk '{print $6}'`

	echo "$start_time;$end_time;$bin;$mode;$workload;$duration;$size;$queue_size;$threads;$run;$sprob;$cprob;$warmup_retval;$nrequests;$speed;$elapsed;$rps;$kbps;$avgrqsz;$avgqusz;$util;$cpu_user;$cpu_nice;$cpu_system;$cpu_iowait;$cpu_steal;$cpu_idle;$cached_before;$cached_after" >> results.csv
}

mkdir -p iostat
rm -Rf iostat/*

mkdir -p results
rm -Rf results/*

rm -f results.csv

echo "start;end;test;mode;workload;duration;size;queue_size;threads;run;start_prob;cont_prob;warmup;nrequests;speed;elapsed;rps;kbps;avgrqsz;avgqusz;util;cpu_user;cpu_nice;cpu_system;cpu_iowait;cpu_steal;cpu_idle;cached_before;cached_after" >> results.csv

# 128GB 16GB 2GB
for s in 16384 131072 2048; do

	rm -Rf results/$s
	mkdir results/$s

	rm -Rf iostat/$s
	mkdir iostat/$s

	# generate random file of given size (in MBs)
	fname=$1/prefetch-$s
	rm -f $fname
	dd if=/dev/urandom of=$fname bs=1M count=$s > /dev/null 2>&1
	sync

	# 3 runs for each test
	for r in `seq 1 3`; do

		for m in DIRECT BUFFERED; do

			for q in 1 8 32 128 1024; do

				# RANDOM

				# libaio and simple fadvise does not allow/need number of threads
				run_test aio-test $fname $m RANDOM $TIME $q "" "" "" $s $r

				if [ "$m" == "BUFFERED" ]; then
					run_test fadvise-test $fname $m RANDOM $TIME $q "" "" "" $s $r
				fi

				# posix-aio, pthreads and pthreads-fadvise do need number of threads
				for t in 1 8 16 32; do

					if [ $q -lt $t ]; then
						continue
					fi

					run_test paio-test $fname $m RANDOM $TIME $q $t "" "" $s $r
					run_test pthread-test $fname $m RANDOM $TIME $q $t "" "" $s $r

					if [ "$m" == "BUFFERED" ]; then
						run_test pthread-fadvise-test $fname $m RANDOM $TIME $q $t "" "" $s $r
					fi

				done

				# SEQUENTIAL

				# start probability
				for sp in 0.01 0.10; do

					# continuation probability
					for cp in 0.1 0.5 0.9; do

						# libaio does not allow/need number of threads
						run_test aio-test $fname $m SEQUENTIAL $TIME $q "" $sp $cp $s $r

						if [ "$m" == "BUFFERED" ]; then
							run_test fadvise-test $fname $m SEQUENTIAL $TIME $q "" $sp $cp $s $r
						fi

						# posix-aio and pthreads do need number of threads
						for t in 1 8 16 32; do

							if [ $q -lt $t ]; then
								continue
							fi

							run_test paio-test $fname $m SEQUENTIAL $TIME $q $t $sp $cp $s $r
							run_test pthread-test $fname $m SEQUENTIAL $TIME $q $t $sp $cp $s $r

							if [ "$m" == "BUFFERED" ]; then
								run_test pthread-fadvise-test $fname $m SEQUENTIAL $TIME $q $t $sp $cp $s $r
							fi

						done

					done

				done

			done

		done

	done

	# remove the test file
	rm $fname

done

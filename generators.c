#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "generators.h"

/* random pattern */

void
random_init(random_state_t *state, unsigned long int npages)
{
	struct timeval tv;

	memset(state, 0, sizeof(random_state_t));
	state->npages = npages;

	gettimeofday(&tv, NULL);
	srand48((long int) (tv.tv_sec * 1000000 + tv.tv_usec));
}

unsigned long int
random_next(void *data)
{
	random_state_t *state = (random_state_t *) data;

	return lrand48() % state->npages;
}

/* sequential pattern, with per-page probability */

void
sequential_init(sequential_state_t *state, unsigned long int npages, float start_prob, float cont_prob)
{
	struct timeval tv;

	memset(state, 0, sizeof(sequential_state_t));
	state->npages = npages;
	state->start_probability = start_prob;
	state->continue_probability = cont_prob;

	/* simply seed it with address of the state (not great, but meh) */
	gettimeofday(&tv, NULL);
	srand48((long int) (tv.tv_sec * 1000000 + tv.tv_usec));
}

unsigned long int
sequential_next(void *data)
{
	sequential_state_t *state = (sequential_state_t *) data;

	/*
	 * First assume the preceding page was accessed, and let's see if
	 * we should automatically 'continue' with this one (essentially
	 * scanning a small part of the file sequentially).
	 */
	if (drand48() < state->continue_probability)
		return ((state->next++) % state->npages);

	/*
	 * OK, so let's look for the next page using the start probability.
	 */
	while (drand48() >= state->start_probability)
		state->next++;

	return (state->next % state->npages);
}
